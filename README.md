mkvmerge
========

Librairie NodeJs servant à l'implémentation (wrapper) de certaines fonctions de l'outils bien connu mkvmerge.


Description
-----------

Pour le moment, seule la fonction de conversion de fichier vidéos au format mkv est implémentée.

Utilisation
-----------

`mkvmerge("/path/to/input/file.avi", "/path/to/output/file.mkv", , function(err, exitcode) { ... })`
