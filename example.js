#!/usr/bin/env node

var util = require("util"),
Mkvmerge = require("./");

var mkvmerge = new Mkvmerge();

mkvmerge.convert(
	"/home/chris/Téléchargements/[ www.xxxx ] film.S01E03.FRENCH.WEB-DL.XviD-ARK01.avi",
	 "/home/chris/Téléchargements/film.mkv",
	function(errors, running, exitcode, progress) {
		if( exitcode == 0 && running == false ) {
				console.log('Conversion terminée avec succès');
		}
		else if( running == false ) {
				console.error('Problème de conversion (' + JSON.stringify(errors) + ')');
		}
		else {
				console.log('Progression en cours ' + progress + '%');
		}
		
	}
);

