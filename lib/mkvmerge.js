var child_process = require("child_process");
var fs = require('fs');

var Mkvmerge = function(progress, errors) {

	this.progress = progress;
	this.errors = errors;
	this.running = false;
	//this.out = '';
	this.exitcode = -1;
	this.key = "";
	this.desc = "";
	this.convert = function (inputfile, outputfile, callbackupdate) {

		var spawn = require('child_process').spawn;
		//var test = spawn("mkvmerge", [ "-o", outputfile, inputfile],{ encoding: 'utf8', env : process.env });  // detached : true {timeout : 0, maxBuffer: 30000000, encoding: 'utf8', env: {LANG: 'fr_FR.UTF-8'}});
		var test = spawn("mkvmerge", [ "-o", outputfile, inputfile],{ encoding: 'utf8', env: {LANG: 'fr_FR.UTF-8'} }); 
		var t = this;
		//if( test.pid > 0)
			
			//callbackstart(null);
		//else
		//	callbackstart('error');
		//callbackstart(null);
		test.stdout.on('data', function (data) {
			console.log('stdout: ' + data);
			//this.out += data.toString();
			this.running = true;
			var patt = new RegExp('.*Progression : [0-9]{1,3}.*','gi');
			
			var dataval = data.toString(),
			lines = dataval.split(/(\r?\n)/g);
			for (var i=0; i<=lines.length; i++) {
				var mydata = lines[i];
				if( patt.test(mydata) ) {				
					var progress = mydata.replace(/.*Progression : ([0-9]{1,3}).*/,'$1');
					progress = progress.replace(/[\n]/gi, "" );
					progress = progress.replace(/[\r]/gi, "" );
					if( progress.toString().match("\\d+") ) {
						t.update(t.running, t.exitcode, progress, callbackupdate);
					}
				}
			}
		});

		test.stderr.on('data', function (data) {
			//console.log('stderr: ' + data);
		  if( t.errors == null )
				t.errors = "";
		  t.errors += data.toString();
		});

		test.on('exit', function (code, signal) {
			//t.update(false, code, this.progress);
		});

		test.on('close', function (code, signal) {
		  //console.log('child process closed with code ' + code + ' (' + signal + ')');
		  t.update(false, code, t.progress, callbackupdate);
		});

		test.on('err', function (err) {
			//console.log('err: ' + err);
			t.errors += err.toString();
			//console.log('child process err with code : ' + err);
			t.update(false, 2, t.progress, callbackupdate);
		});
		this.running = true;
	}
	
	this.update = function(running, exitcode, progress, callbackupdate) {
		this.running = running;
		this.exitcode = exitcode;
		//console.log('!! exitcode : ' + JSON.stringify(exitcode));
		this.progress = parseInt(progress);
		callbackupdate(this.errors, this.running, this.exitcode, this.progress);
	}
};

module.exports = Mkvmerge;
